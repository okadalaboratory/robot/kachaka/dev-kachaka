# kachakaをDockerを使ってROS2から動かす
![screenshot](https://gitlab.com/okadalaboratory/robot/kachaka/dev-kachaka/-/raw/fig/system.png?ref_type=heads)


kachakaをDockerを使ってROS2から動かしてみます。

参考にしたサイト<br>
[スマートファニチャー・プラットフォーム「カチャカ」 APIマニュアル](https://github.com/pf-robotics/kachaka-api)

[kachakaをROS2のnavigation2から動かす](https://qiita.com/terakoji-pfr/items/0f1535b45fda58edad83)


## 環境
- Kachaka本体
    - バージョン３.014
- PC
    - Ubuntu22.04(推奨)

## ホストPCの準備
## Dockerのインストール
お使いのホストPCにDockerをインストールします。

## kachaka apiのインストール
お使いのホストPCにkachaka apiをインストールします。

ここでは、ホストPCのホームディレクトリに kachaka api用のディレクトリを作り、そこに kachakaの公式サイトからapiをダウンロードします。

ディレクトリ名はkachakaとしましたが、任意の名前で作ることが可能です。
```
(host)$ cd ~
(host)$ mkdir kachaka
(host)$ cd kachaka
(host)$ git clone https://github.com/pf-robotics/kachaka-api.git
```

## kachaka開発用Dockerイメージの作成
### リポジトリのダウンロード
本リポジトリをダウンロードします。
```
(host)$ cd ~
(host)$ git clone https://gitlab.com/okadalaboratory/robot/kachaka/dev-kachaka.git
```

### .envファイルの修正
docker compose の設定ファイルである、 .env ファイルをご自分の環境に合わせて修正して下さい。

```
(host)$ cd ~/dev-kachaka/docker
(host)$ cat .env
USER_NAME=[ユーザ名]
GROUP_NAME=[グループ名]
UID=[ユーザID]
GID=[グループID]
PASSWORD=[パスワード]
WORKSPACE_DIR=./ros2_ws
```
ここで、WORKSPACE_DIRはDockerコンテナとホストPCで共有するディレクトリです。
自信がros2を開発するワークスペースなどを指定してください。

### Dockerイメージの作成
下記のコマンドでDockerイメージを作成します。
```
(host)$ cd ~/dev-kachaka/docker
(host)$ docker compose build
```
エラーが出なければ kachaka開発用のDockerイメージの作成は成功です。


## 動作確認
### Kachakaの起動
Kachakaの電源スイッチを押して、起動状態にしてください。

### ホストPC側での操作
以下のコマンドでros2とgPRCのブリッジをスタートします。

kachakaのIPアドレスはスマートフォンアプリから確認できます。
```
(host)$ cd ~/kachaka/kachaka-api/tools/ros2_bridge
(host)$ ./start_bridge.sh <kachakaのIPアドレス>
```

### Dockerコンテナの起動
```
(host)$ cd ~/dev-kachaka/docker
(host)$ docker compose up -d
[+] Running 2/2
 ✔ Network docker_default  Created     0.1s
 ✔ Container kachaka-humble  Started   2.2s
 ```

コンテナが正常に起動したら、下記のコマンドでコンテナに入ります。
```
(host)$ docker compose exec kachaka-humble /bin/bash
```

### kachakaのトピックの確認
コンテナに入ったら、下記のコマンドでROS2のトピックを確認してみてください。

下記のようにkachakaのトピックが表示されていれば正常です。
```
(docker)$ ros2 topic list
/goal_pose
/kachaka/front_camera/camera_info
/kachaka/front_camera/image_raw
/kachaka/front_camera/image_raw/compressed
/kachaka/imu/imu
/kachaka/layout/locations/list
/kachaka/layout/shelves/list
/kachaka/lidar/scan
/kachaka/manual_control/cmd_vel
/kachaka/mapping/map
/kachaka/object_detection/result
/kachaka/odometry/odometry
/kachaka/robot_info/version
/kachaka_description/joint_states
/kachaka_description/robot_description
/parameter_events
/rosout
/tf
/tf_static
```

###　トピックの読み取り(echo)と利用(pub)
kachakaのオドメトリ(/kachaka/odometry/odometry)は下記のコマンドで読み取れます。
```
(docker)$ ros2 topic echo /kachaka/odometry/odometry
---
header:
  stamp:
    sec: 1695695832
    nanosec: 515094519
  frame_id: odom
child_frame_id: ''
pose:
  pose:
    position:
      x: 0.09743609985928547
      y: -0.009839604173889729
      z: 0.0
    orientation:
      x: 0.0
      y: 0.0
      z: 0.026016736016508665
      w: 0.9996615074349153
  covariance:
  - 8061048933576952.0
...
...
```

次にtopicをpublishすることでkachakaを動かしてみます。
下記のコマンドはその場で0.2[rad/s]で回転するコマンドです。
```
(docker)$ ros2 topic pub /kachaka/manual_control/cmd_vel geometry_msgs/msg/Twist '{linear: {x: 0.0}, angular: {z: 0.2}}'
```
kachakaがその場で回転すれば正常です。


### rviz2で可視化
可視化ツールのrviz2で動いているkachakaを表示してみます。
```
(docker)$ rviz2 -d ~/kachaka_ws/src/kachaka_description/config/kachaka.rviz
```
rvizのウインドウのメニューの2D Goal Poseから移動させたいgoalを指定するとロボットが動き出します。
周囲の環境に注意してkachakaを動かしてみてください。

![screenshot](https://gitlab.com/okadalaboratory/robot/kachaka/dev-kachaka/-/raw/fig/Screenshot%20from%202023-09-26%2011-29-30.png?ref_type=heads)


## Navigation2
ros2のブリッジが動いている状態で、下記のコマンドを実行します。
```
(docker)$ ros2 launch kachaka_bringup navigation_launch.py
```

別のターミナルからrviz2で可視化します。
```
(docker)$ rviz2 -d ~/kachaka_ws/src/kachaka_bringup/share/kachaka_bringup/rviz/kachaka-nav.rviz
```

![screenshot](https://gitlab.com/okadalaboratory/robot/kachaka/dev-kachaka/-/raw/fig/Screenshot%20from%202023-09-26%2014-25-59.png?ref_type=heads)

## 公式サンプルの動かし方
[公式サンプル](https://github.com/pf-robotics/kachaka-api/tree/main/ros2/demos)

### [kachaka_follow](https://github.com/pf-robotics/kachaka-api/tree/main/ros2/demos/kachaka_follow)

LIDARを使って最も近いオブジェクトに向かって移動する例です。cmd_velとLaserScanの使い方の説明が含まれています。

kachakaが歩いている人について動きます。

ros2のブリッジをスタートします。
```
(host)$ cd ~/kachaka/kachaka-api/tools/ros2_bridge
(host)$ ./start_bridge.sh <kachakaのIPアドレス>
```

別のターミナルでDockerコンテナを起動します
```
(host)$ cd ~/dev-kachaka/docker
(host)$ docker compose up -d
```
コンテナが正常に起動したら、下記のコマンドでコンテナに入ります。
```
(host)$ docker compose exec kachaka-humble /bin/bash
```

Dockerコンテナのターミナルで下記のコマンドを実行します。
```
(docker)ros2 run kachaka_follow follow
```

### [kachaka_speak](https://github.com/pf-robotics/kachaka-api/tree/main/ros2/demos/kachaka_speak)
kachakaで発話するためのデモプログラムです。

下記のコマンドを実行します。
```
(docker)ros2 run kachaka_speak speak
```

### [kachaka_smart_speaker](https://github.com/pf-robotics/kachaka-api/tree/main/ros2/demos/kachaka_smart_speaker)
スマートスピーカー連携サンプル


### [kachaka_vision](https://github.com/pf-robotics/kachaka-api/tree/main/ros2/demos/kachaka_vision)
kachakaのフロントRGBカメラでopenposeを使い、手のポーズを検出する例です。

下記のコマンドを実行します。
```
(docker)ros2 launch kachaka_vision hand_recognition_launch.py
```

認識結果は下記のコマンドで確認できます。
```
(docker)ros2 run rqt_image_view rqt_image_view /hand_recognition_node/output_image
```
